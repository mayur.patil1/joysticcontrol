package com.mayurflytbase.joystickcontrol.model;

import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

public class NavRequestMove {

    @SerializedName("vx")
    @Expose
    private Double vx;
    @SerializedName("vy")
    @Expose
    private Double vy;
    @SerializedName("vz")
    @Expose
    private Double vz;
    @SerializedName("yaw_rate")
    @Expose
    private Double yawRate;
    @SerializedName("tolerance")
    @Expose
    private Double tolerance;
    @SerializedName("async")
    @Expose
    private Boolean async;
    @SerializedName("relative")
    @Expose
    private Boolean relative;
    @SerializedName("yaw_rate_valid")
    @Expose
    private Boolean yawRateValid;
    @SerializedName("body_frame")
    @Expose
    private Boolean bodyFrame;

    public NavRequestMove(Double vx, Double vy, Double vz, Double yawRate) {
        this.vx = vx;
        this.vy = vy;
        this.vz = vz;
        this.yawRate = yawRate;
        this.tolerance = 2.00;
        this.async = true;
        this.relative = false;
        this.yawRateValid = true;
        this.bodyFrame = true;
    }

    public Double getVx() {
        return vx;
    }

    public void setVx(Double vx) {
        this.vx = vx;
    }

    public Double getVy() {
        return vy;
    }

    public void setVy(Double vy) {
        this.vy = vy;
    }

    public Double getVz() {
        return vz;
    }

    public void setVz(Double vz) {
        this.vz = vz;
    }

    public Double getYawRate() {
        return yawRate;
    }

    public void setYawRate(Double yawRate) {
        this.yawRate = yawRate;
    }

    public Double getTolerance() {
        return tolerance;
    }

    public void setTolerance(Double tolerance) {
        this.tolerance = tolerance;
    }

    public Boolean getAsync() {
        return async;
    }

    public void setAsync(Boolean async) {
        this.async = async;
    }

    public Boolean getRelative() {
        return relative;
    }

    public void setRelative(Boolean relative) {
        this.relative = relative;
    }

    public Boolean getYawRateValid() {
        return yawRateValid;
    }

    public void setYawRateValid(Boolean yawRateValid) {
        this.yawRateValid = yawRateValid;
    }

    public Boolean getBodyFrame() {
        return bodyFrame;
    }

    public void setBodyFrame(Boolean bodyFrame) {
        this.bodyFrame = bodyFrame;
    }

    @Override
    public String toString() {
        return "NavRequestMove{" +
                "vx=" + vx +
                ", vy=" + vy +
                ", vz=" + vz +
                ", yawRate=" + yawRate +
                ", tolerance=" + tolerance +
                ", async=" + async +
                ", relative=" + relative +
                ", yawRateValid=" + yawRateValid +
                ", bodyFrame=" + bodyFrame +
                '}';
    }
}
