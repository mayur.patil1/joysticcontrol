package com.mayurflytbase.joystickcontrol.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NavRequestTakeoff {

    @SerializedName("takeoff_alt")
    @Expose
    private Double takeoffAlt;

    public NavRequestTakeoff(Double takeoffAlt) {
        this.takeoffAlt = takeoffAlt;
    }

    public Double getTakeoffAlt() {
        return takeoffAlt;
    }

    public void setTakeoffAlt(Double takeoffAlt) {
        this.takeoffAlt = takeoffAlt;
    }

}