package com.mayurflytbase.joystickcontrol.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NavRequestLand {
    @SerializedName("async")
    @Expose
    private Boolean async;

    public NavRequestLand(Boolean async) {
        this.async = async;
    }

    public Boolean getAsync() {
        return async;
    }

    public void setAsync(Boolean async) {
        this.async = async;
    }
}
