package com.mayurflytbase.joystickcontrol.service;

import com.mayurflytbase.joystickcontrol.model.NavRequestLand;
import com.mayurflytbase.joystickcontrol.model.NavRequestMove;
import com.mayurflytbase.joystickcontrol.model.NavRequestTakeoff;
import com.mayurflytbase.joystickcontrol.model.NavResponse;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface NavigationService {

    //@Headers({"Authorization:Token b0320274e82086a305a0bb1198f17e711782a9a3","VehicleID:2BnW2BPv"})
    @POST("take_off")
    Observable<NavResponse> takeOff(@Body NavRequestTakeoff body);

    @POST("land")
    Observable<NavResponse> land(@Body NavRequestLand body);

    @POST("velocity_set")
    Observable<NavResponse> move(@Body NavRequestMove body);

    @GET("position_hold")
    Observable<NavResponse> holdPosition();

}
