package com.mayurflytbase.joystickcontrol.service;

import android.app.Application;
import android.util.Log;

import com.mayurflytbase.joystickcontrol.model.NavRequestLand;
import com.mayurflytbase.joystickcontrol.model.NavRequestMove;
import com.mayurflytbase.joystickcontrol.model.NavRequestTakeoff;
import com.mayurflytbase.joystickcontrol.model.NavResponse;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class NavigationRepository {

    private Application application;
    private Observable<NavResponse> navResponseObservable;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public NavigationRepository(Application application) {
        this.application = application;
    }


    public void takeoffDrone(NavRequestTakeoff navRequestTakeoff) {
        NavigationService navigationService = RetrofitInstance.getNavService();
        navResponseObservable = navigationService.takeOff(navRequestTakeoff);
        compositeDisposable.add(navResponseObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<NavResponse>() {

                    @Override
                    public void onNext(NavResponse navResponse) {
                        Log.d("takeoff navResponse--> ", navResponse.toString());
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        Log.d("takeoff complete--> ", "true");
                    }
                })
        );

    }

    public void landDrone(NavRequestLand navRequestLand) {
        NavigationService navigationService = RetrofitInstance.getNavService();
        navResponseObservable = navigationService.land(navRequestLand);
        compositeDisposable.add(navResponseObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<NavResponse>() {

                    @Override
                    public void onNext(NavResponse navResponse) {
                        Log.d("navResponse--> ", navResponse.toString());
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        Log.d("landing complete--> ", "true");
                    }
                })
        );

    }

    public void moveDrone(NavRequestMove navRequestMove) {
        NavigationService navigationService = RetrofitInstance.getNavService();
        navResponseObservable = navigationService.move(navRequestMove);
        compositeDisposable.add(navResponseObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<NavResponse>() {

                    @Override
                    public void onNext(NavResponse navResponse) {
                        Log.d("move navResponse--> ", navResponse.toString());
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        Log.d("move complete--> ", "true");
                    }
                })
        );
    }

    public void holdDronePosition() {
        NavigationService navigationService = RetrofitInstance.getNavService();
        navResponseObservable = navigationService.holdPosition();
        compositeDisposable.add(navResponseObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<NavResponse>() {

                    @Override
                    public void onNext(NavResponse navResponse) {
                        Log.d("move navResponse--> ", navResponse.toString());
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        Log.d("move complete--> ", "true");
                    }
                })
        );
    }
}

