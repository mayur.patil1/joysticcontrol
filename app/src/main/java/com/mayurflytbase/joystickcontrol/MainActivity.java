package com.mayurflytbase.joystickcontrol;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.mayurflytbase.joystickcontrol.model.NavRequestLand;
import com.mayurflytbase.joystickcontrol.model.NavRequestMove;
import com.mayurflytbase.joystickcontrol.model.NavRequestTakeoff;
import com.mayurflytbase.joystickcontrol.service.NavigationRepository;

public class MainActivity extends AppCompatActivity {

    private NavigationRepository navigationRepository;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        navigationRepository = new NavigationRepository(this.getApplication());

        /*NavRequestTakeoff navRequestTakeoff = new NavRequestTakeoff(20.00);
        navigationRepository.takeoffDrone(navRequestTakeoff);

        NavRequestLand navRequestLand = new NavRequestLand(false);
        navigationRepository.landDrone(navRequestLand);*/

        //Move Right
        /*NavRequestMove navRequestMove = new NavRequestMove(0.00,2.00,0.00,0.00);
        navigationRepository.moveDrone(navRequestMove);*/

        //Move Left
        /*NavRequestMove navRequestMove = new NavRequestMove(0.00,-2.00,0.00,0.00);
        navigationRepository.moveDrone(navRequestMove);*/

        //Move Forward
        /*NavRequestMove navRequestMove = new NavRequestMove(2.00,0.00,0.00,0.00);
        navigationRepository.moveDrone(navRequestMove);*/

        //Move Back
        /*NavRequestMove navRequestMove = new NavRequestMove(-2.00,0.00,0.00,0.00);
        navigationRepository.moveDrone(navRequestMove);*/

        //Move Down
        /*NavRequestMove navRequestMove = new NavRequestMove(0.00,0.00,1.00,0.00);
        navigationRepository.moveDrone(navRequestMove);*/

        //Move Up
        /*NavRequestMove navRequestMove = new NavRequestMove(0.00,0.00,-1.00,0.00);
        navigationRepository.moveDrone(navRequestMove);*/

        //Hold Position
        //navigationRepository.holdDronePosition();

    }
}